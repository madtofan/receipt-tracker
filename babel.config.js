module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          components: './src/components',
          layouts: './src/layouts',
          constants: './src/constants',
          routes: './src/routes',
          utils: './src/utils',
          stores: './src/stores'
        },
      },
    ],
  ],
}