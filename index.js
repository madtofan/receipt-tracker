/**
 * @format
 */

import { Loading } from 'components/Loading';
import * as React from 'react';
import { AppRegistry } from 'react-native';
import 'react-native-get-random-values';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { reduxStore } from 'stores/redux';
import App from './App';
import { name as appName } from './app.json';

const persistor = persistStore(reduxStore);

export class AppRoot extends React.Component {
    render() {
        return (
            <Provider store={reduxStore}>
                <PersistGate loading={<Loading isVisible={true} />} persistor={persistor}>
                    <App />
                </PersistGate>
            </Provider>
        );
    }
}

AppRegistry.registerComponent(appName, () => AppRoot);
