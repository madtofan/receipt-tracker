import {
  BROWSE_PATH,
  DASHBOARD_PATH,
  RECEIPTS_PATH,
  ROOT_PATH,
  SCANNER_PATH,
} from 'constants/paths';
import * as React from 'react';
import NavigationButtons from '../components/NavigationButtons';

interface Props {}

const NavigationContainer: React.FC<Props> = ({}) => {
  return (
    <>
      <NavigationButtons
        icon="insert-invitation"
        path={ROOT_PATH}
        textButton="Home"
      />
      <NavigationButtons
        icon="view-carousel"
        path={BROWSE_PATH}
        textButton="Slide"
      />
      <NavigationButtons
        icon="dashboard"
        path={DASHBOARD_PATH}
        textButton="Dashboard"
      />
      <NavigationButtons
        icon="receipt"
        path={RECEIPTS_PATH}
        textButton="Receipts"
      />
      <NavigationButtons
        icon="add-a-photo"
        path={SCANNER_PATH}
        textButton="Scanner"
      />
    </>
  );
};

export default NavigationContainer;
