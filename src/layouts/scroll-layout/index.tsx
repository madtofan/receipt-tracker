import { GLOBAL_STYLE } from 'constants/styling';
import * as React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import NavigationContainer from './defaults/NavigationContainer';

interface Props {
  navigationContainer?: React.FC;
  scrollable?: boolean;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%',
  },
  wrapper: {
    width: '100%',
    flexShrink: 1,
    backgroundColor: 'white',
  },
  navButtonsContainer: {
    backgroundColor: 'grey',
    height: 49,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const ScrollLayout: React.FC<Props> = ({
  children,
  navigationContainer = <NavigationContainer />,
  scrollable = true,
}) => {
  return (
    <>
      <SafeAreaView style={styles.container}>
        {scrollable ? (
          <ScrollView
            style={styles.wrapper}
            bounces={true}
            pinchGestureEnabled={false}>
            {children}
          </ScrollView>
        ) : (
          <View style={styles.wrapper}>{children}</View>
        )}
      </SafeAreaView>
      <View style={[styles.navButtonsContainer, , GLOBAL_STYLE.shadow]}>
        {navigationContainer}
      </View>
    </>
  );
};

export default ScrollLayout;
