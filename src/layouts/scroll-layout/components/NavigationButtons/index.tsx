import { StyledText } from 'components/StyledText';
import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { RouteComponentProps, withRouter } from 'react-router-native';

interface Props extends RouteComponentProps {
  path?: string;
  icon: string;
  textButton?: string;
  onPress?: () => void;
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
  },
});

const NavigationButtons: React.FC<Props> = ({
  history,
  path = '',
  icon,
  textButton,
  onPress,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={styles.container}
      onPress={
        onPress
          ? onPress
          : () => {
              history.push(path);
            }
      }>
      <MaterialIcons size={18} color={'black'} name={icon} />
      {textButton && (
        <StyledText
          style={[
            {
              color: 'black',
              justifyContent: 'center',
              fontSize: 10,
              paddingBottom: 5,
            },
          ]}>
          {textButton}
        </StyledText>
      )}
    </TouchableOpacity>
  );
};

export default withRouter(NavigationButtons);
