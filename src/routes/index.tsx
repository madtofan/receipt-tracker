import {
  BROWSE_PATH,
  DASHBOARD_PATH,
  RECEIPTS_PATH,
  ROOT_PATH,
  SCANNER_PATH,
} from 'constants/paths';
import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  BackButton,
  NativeRouter as Router,
  Route,
  Switch,
} from 'react-router-native';
import Home from 'routes/Home';
import AddReceipt from './AddReceipt';
import BrowseReceipt from './BrowseReceipt';
import Dashboard from './Dashboard';
import DocumentScanner from './Scanner/components/DocumentScanner';

interface Props {}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
});

const Routes: React.FC<Props> = ({}) => {
  return (
    <View style={styles.container}>
      <Router>
        <BackButton>
          <Switch>
            <Route exact path={ROOT_PATH} component={Home} />
            <Route exact path={BROWSE_PATH} component={BrowseReceipt} />
            <Route exact path={DASHBOARD_PATH} component={Dashboard} />
            <Route exact path={RECEIPTS_PATH} component={AddReceipt} />
            <Route exact path={SCANNER_PATH} component={DocumentScanner} />
          </Switch>
        </BackButton>
      </Router>
    </View>
  );
};

export default Routes;
