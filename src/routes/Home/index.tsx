import { StyledText } from 'components/StyledText';
import ScrollLayout from 'layouts/scroll-layout';
import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { realmStore } from 'stores/realm';
import { IReceiptModel } from 'stores/realm/models/receiptModel';

interface Props {}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  header: {
    backgroundColor: 'white',
    height: 49,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  content: {
    flexGrow: 1,
  },
});

const Home: React.FC<Props> = ({}) => {
  const [receipts, setReceipts] = React.useState<IReceiptModel[]>(null);

  React.useEffect(() => {
    setReceipts(Array.from(realmStore.receipt.retrieveAllReceipts()));
  }, []);

  return (
    <ScrollLayout>
      <View style={styles.container}>
        <View style={styles.header}>
          <StyledText>Home</StyledText>
        </View>
        <View style={styles.content}>
          {receipts
            ? receipts.map((item) => (
                <View key={item.receiptId}>
                  <View
                    key={item.receiptId}
                    style={{ backgroundColor: 'white', padding: 20 }}>
                    <StyledText>{`Id: ${item.receiptId}`}</StyledText>
                    <StyledText>{`Store Name: ${item.storeName}`}</StyledText>
                    <StyledText>{`Description: ${item.description}`}</StyledText>
                    <StyledText>{`Last Updated: ${item.lastUpdated}`}</StyledText>
                  </View>
                  <View
                    style={{
                      height: 0.5,
                      width: '100%',
                      backgroundColor: '#000',
                    }}
                  />
                </View>
              ))
            : null}
        </View>
      </View>
    </ScrollLayout>
  );
};

export default Home;
