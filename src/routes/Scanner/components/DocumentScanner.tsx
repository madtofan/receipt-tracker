import * as React from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Scanner, {
  DetectedRectangle,
  DeviceSetupCallbackProps,
  Filters,
  RectangleOverlay,
} from 'react-native-rectangle-scanner';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ScannerFilters from './ScannerFilters';

interface Props {
  cameraIsOn?: boolean;
  onLayout?: (event: Event) => void;
  onSkip?: () => void;
  onCancel?: () => void;
  onPictureTaken?: (event: Event) => void;
  onPictureProcessed?: (event: Event) => void;
  hideSkip?: boolean;
  initialFilterId?: number;
  onFilterIdChange?: (id: number) => void;
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    height: 70,
    justifyContent: 'center',
    width: 65,
  },
  buttonActionGroup: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  buttonBottomContainer: {
    alignItems: 'flex-end',
    bottom: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    left: 25,
    position: 'absolute',
    right: 25,
  },
  buttonContainer: {
    alignItems: 'flex-end',
    bottom: 25,
    flexDirection: 'column',
    justifyContent: 'space-between',
    position: 'absolute',
    right: 25,
    top: 25,
  },
  buttonGroup: {
    backgroundColor: '#00000080',
    borderRadius: 17,
  },
  buttonIcon: {
    color: 'white',
    fontSize: 22,
    marginBottom: 3,
    textAlign: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 13,
  },
  buttonTopContainer: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between',
    left: 25,
    position: 'absolute',
    right: 25,
    top: 40,
  },
  cameraButton: {
    backgroundColor: 'white',
    borderRadius: 50,
    flex: 1,
    margin: 3,
  },
  cameraNotAvailableContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 15,
  },
  cameraNotAvailableText: {
    color: 'white',
    fontSize: 25,
    textAlign: 'center',
  },
  cameraOutline: {
    borderColor: 'white',
    borderRadius: 50,
    borderWidth: 3,
    height: 70,
    width: 70,
  },
  container: {
    backgroundColor: 'black',
    flex: 1,
  },
  flashControl: {
    alignItems: 'center',
    borderRadius: 30,
    height: 50,
    justifyContent: 'center',
    margin: 8,
    paddingTop: 7,
    width: 50,
  },
  loadingCameraMessage: {
    color: 'white',
    fontSize: 18,
    marginTop: 10,
    textAlign: 'center',
  },
  loadingContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  overlay: {
    bottom: 0,
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  processingContainer: {
    alignItems: 'center',
    backgroundColor: 'rgba(220, 220, 220, 0.7)',
    borderRadius: 16,
    height: 140,
    justifyContent: 'center',
    width: 200,
  },
  scanner: {
    flex: 1,
  },
});

interface DeviceDetail extends DeviceSetupCallbackProps {
  initialized: boolean;
}

interface ScannerState {
  flashEnabled: boolean;
  showScannerView: boolean;
  didLoadInitialLayout: boolean;
  filterId: number;
  detectedRectangle: DetectedRectangle;
  isMultiTasking: boolean;
  loadingCamera: boolean;
  processingImage: boolean;
  takingPicture: boolean;
  overlayFlashOpacity: Animated.Value;
  device: DeviceDetail;
}

const DocumentScanner: React.FC<Props> = ({
  cameraIsOn = undefined,
  onLayout = (event: Event) => {},
  onSkip = () => {},
  onCancel = () => {},
  onPictureTaken = (event: Event) => {},
  onPictureProcessed = (event: Event) => {},
  onFilterIdChange = (id: number) => {},
  hideSkip = false,
  initialFilterId = Filters.PLATFORM_DEFAULT_FILTER_ID,
}) => {
  const [scannerState, setScannerState] = React.useState<ScannerState>({
    flashEnabled: false,
    showScannerView: false,
    didLoadInitialLayout: false,
    filterId: initialFilterId,
    detectedRectangle: {
      bottomLeft: { x: 0, y: 0 },
      bottomRight: { x: 0, y: 0 },
      dimensions: { height: 0, width: 0 },
      topLeft: { x: 0, y: 0 },
      topRight: { x: 0, y: 0 },
    },
    isMultiTasking: false,
    loadingCamera: true,
    processingImage: false,
    takingPicture: false,
    overlayFlashOpacity: new Animated.Value(0),
    device: {
      initialized: false,
      hasCamera: false,
      permissionToUseCamera: false,
      flashIsAvailable: false,
      previewHeightPercent: 1,
      previewWidthPercent: 1,
    },
  });

  const [imageProcessorTimeout, setImageProcessorTimeout] = React.useState<
    ReturnType<typeof setTimeout>
  >(null);

  React.useEffect(() => {
    if (scannerState.didLoadInitialLayout && !scannerState.isMultiTasking) {
      turnOnCamera();
    }

    return () => {
      clearTimeout(imageProcessorTimeout);
    };
  });

  React.useEffect(() => {
    componentUpdated();
  }, [scannerState]);

  const componentUpdated = () => {
    if (scannerState.didLoadInitialLayout) {
      if (scannerState.isMultiTasking) return turnOffCamera(true);
      if (scannerState.device.initialized) {
        if (!scannerState.device.hasCamera) return turnOffCamera();
        if (!scannerState.device.permissionToUseCamera) return turnOffCamera();
      }

      if (cameraIsOn === true && !scannerState.showScannerView) {
        return turnOnCamera();
      }

      if (cameraIsOn === false && scannerState.showScannerView) {
        return turnOffCamera(true);
      }

      if (cameraIsOn === undefined) {
        return turnOnCamera();
      }
    }
    return null;
  };

  const cameraRef = React.useRef(null);

  // The picture was captured but still needs to be processed.
  const onScannerPictureTaken = (event: Event) => {
    setScannerState((prevState) => ({ ...prevState, takingPicture: false }));
    onPictureTaken(event);
  };

  // The picture was taken and cached. You can now go on to using it.
  const onScannerPictureProcessed = (event: Event) => {
    onPictureProcessed(event);
    setScannerState((prevState) => ({
      ...prevState,
      takingPicture: false,
      processingImage: false,
      showScannerView: cameraIsOn || false,
    }));
  };

  // Will show the camera view which will setup the camera and start it.
  // Expect the onDeviceSetup callback to be called
  const turnOnCamera = () => {
    if (!scannerState.showScannerView) {
      setScannerState((prevState) => ({
        ...prevState,
        showScannerView: true,
        loadingCamera: true,
      }));
    }
  };

  // Hides the camera view. If the camera view was shown and onDeviceSetup was called,
  // but no camera was found, it will not uninitialize the camera state.
  const turnOffCamera = (shouldUninitializeCamera = false) => {
    if (shouldUninitializeCamera && scannerState.device.initialized) {
      setScannerState((prevState) => ({
        ...prevState,
        showScannerView: false,
        device: { ...prevState.device, initialized: false },
      }));
    } else if (scannerState.showScannerView) {
      setScannerState((prevState) => ({
        ...prevState,
        showScannerView: false,
      }));
    }
  };

  // Determine why the camera is disabled.
  const getCameraDisabledMessage = () => {
    if (scannerState.isMultiTasking) {
      return 'Camera is not allowed in multi tasking mode.';
    }

    const { device } = scannerState;
    if (device.initialized) {
      if (!device.hasCamera) {
        return 'Could not find a camera on the device.';
      }
      if (!device.permissionToUseCamera) {
        return 'Permission to use camera has not been granted.';
      }
    }
    return 'Failed to set up the camera.';
  };

  // Flashes the screen on capture
  const triggerSnapAnimation = () => {
    Animated.sequence([
      Animated.timing(scannerState.overlayFlashOpacity, {
        toValue: 0.2,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(scannerState.overlayFlashOpacity, {
        toValue: 0,
        duration: 50,
        useNativeDriver: true,
      }),
      Animated.timing(scannerState.overlayFlashOpacity, {
        toValue: 0.6,
        delay: 100,
        duration: 120,
        useNativeDriver: true,
      }),
      Animated.timing(scannerState.overlayFlashOpacity, {
        toValue: 0,
        duration: 90,
        useNativeDriver: true,
      }),
    ]).start();
  };

  // Capture the current frame/rectangle. Triggers the flash animation and shows a
  // loading/processing state. Will not take another picture if already taking a picture.
  const capture = () => {
    if (scannerState.takingPicture) return;
    if (scannerState.processingImage) return;
    setScannerState((prevState) => ({
      ...prevState,
      takingPicture: true,
      processingImage: true,
    }));
    cameraRef.current.capture();
    triggerSnapAnimation();

    // If capture failed, allow for additional captures
    setImageProcessorTimeout(
      setTimeout(() => {
        if (scannerState.takingPicture) {
          setScannerState((prevState) => ({
            ...prevState,
            takingPicture: false,
          }));
        }
      }, 100),
    );
  };

  // Set the camera view filter
  const onScannerFilterIdChange = (id: number) => {
    setScannerState((prevState) => ({ ...prevState, filterId: id }));
    onFilterIdChange(id);
  };

  // Renders the flashlight button. Only shown if the device has a flashlight.
  const renderFlashControl = () => {
    if (!scannerState.device.flashIsAvailable) return null;
    return (
      <TouchableOpacity
        style={[
          styles.flashControl,
          {
            backgroundColor: scannerState.flashEnabled
              ? '#FFFFFF80'
              : '#00000080',
          },
        ]}
        activeOpacity={0.8}
        onPress={() =>
          setScannerState((prevState) => ({
            ...prevState,
            flashEnabled: !scannerState.flashEnabled,
          }))
        }>
        <MaterialIcons
          name="flash-on"
          style={[
            styles.buttonIcon,
            {
              fontSize: 28,
              color: scannerState.flashEnabled ? '#333' : '#FFF',
            },
          ]}
        />
      </TouchableOpacity>
    );
  };

  // Renders the camera controls. This will show controls on the side for large tablet screens
  // or on the bottom for phones. (For small tablets it will adjust the view a little bit).
  const renderCameraControls = () => {
    const dimensions = Dimensions.get('window');
    const aspectRatio = dimensions.height / dimensions.width;
    const isPhone = aspectRatio > 1.6;
    const cameraIsDisabled =
      scannerState.takingPicture || scannerState.processingImage;
    const disabledStyle = { opacity: cameraIsDisabled ? 0.8 : 1 };

    if (!isPhone) {
      if (dimensions.height < 500) {
        return (
          <View style={styles.buttonContainer}>
            <View
              style={[
                styles.buttonActionGroup,
                {
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                  marginBottom: 28,
                },
              ]}>
              {renderFlashControl()}
              <ScannerFilters
                filterId={scannerState.filterId}
                isMobile={isPhone}
                onFilterIdChange={onScannerFilterIdChange}
              />
              {hideSkip ? null : (
                <View style={[styles.buttonGroup, { marginLeft: 8 }]}>
                  <TouchableOpacity
                    style={[styles.button, disabledStyle]}
                    onPress={cameraIsDisabled ? () => null : onSkip}
                    activeOpacity={0.8}>
                    <MaterialIcons
                      name="chevron-right"
                      size={40}
                      color="white"
                      style={styles.buttonIcon}
                    />
                    <Text style={styles.buttonText}>Skip</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
            <View style={[styles.cameraOutline, disabledStyle]}>
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.cameraButton}
                onPress={capture}
              />
            </View>
            <View style={[styles.buttonActionGroup, { marginTop: 28 }]}>
              <View style={styles.buttonGroup}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={onCancel}
                  activeOpacity={0.8}>
                  <MaterialIcons
                    name="lock-outline"
                    size={40}
                    style={styles.buttonIcon}
                  />
                  <Text style={styles.buttonText}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        );
      }
      return (
        <View style={styles.buttonContainer}>
          <View
            style={[
              styles.buttonActionGroup,
              { justifyContent: 'flex-end', marginBottom: 20 },
            ]}>
            {renderFlashControl()}
            <ScannerFilters
              filterId={scannerState.filterId}
              isMobile={isPhone}
              onFilterIdChange={onScannerFilterIdChange}
            />
          </View>
          <View style={[styles.cameraOutline, disabledStyle]}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.cameraButton}
              onPress={capture}
            />
          </View>
          <View style={[styles.buttonActionGroup, { marginTop: 28 }]}>
            <View style={styles.buttonGroup}>
              {hideSkip ? null : (
                <TouchableOpacity
                  style={[styles.button, disabledStyle]}
                  onPress={cameraIsDisabled ? () => null : onSkip}
                  activeOpacity={0.8}>
                  <MaterialIcons
                    name="play-circle-filled"
                    size={40}
                    color="white"
                    style={styles.buttonIcon}
                  />
                  <Text style={styles.buttonText}>Skip</Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.buttonGroup}>
              <TouchableOpacity
                style={styles.button}
                onPress={onCancel}
                activeOpacity={0.8}>
                <MaterialIcons
                  name="lock-outline"
                  size={40}
                  style={styles.buttonIcon}
                />
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }

    return (
      <>
        <View style={styles.buttonBottomContainer}>
          <View style={styles.buttonGroup}>
            <TouchableOpacity
              style={styles.button}
              onPress={onCancel}
              activeOpacity={0.8}>
              <MaterialIcons
                name="lock-outline"
                size={40}
                style={styles.buttonIcon}
              />
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.cameraOutline, disabledStyle]}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.cameraButton}
              onPress={capture}
            />
          </View>
          <View>
            <View
              style={[
                styles.buttonActionGroup,
                {
                  justifyContent: 'flex-end',
                  marginBottom: hideSkip ? 0 : 16,
                },
              ]}>
              <ScannerFilters
                filterId={scannerState.filterId}
                isMobile={isPhone}
                onFilterIdChange={onScannerFilterIdChange}
              />
              {renderFlashControl()}
            </View>
            <View style={styles.buttonGroup}>
              {hideSkip ? null : (
                <TouchableOpacity
                  style={[styles.button, disabledStyle]}
                  onPress={cameraIsDisabled ? () => null : onSkip}
                  activeOpacity={0.8}>
                  <MaterialIcons
                    name="play-circle-filled"
                    size={40}
                    color="white"
                    style={styles.buttonIcon}
                  />
                  <Text style={styles.buttonText}>Skip</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </>
    );
  };

  // Called after the device gets setup. This lets you know some platform specifics
  // like if the device has a camera or flash, or even if you have permission to use the
  // camera. It also includes the aspect ratio correction of the preview
  const onDeviceSetup = (deviceDetails: DeviceSetupCallbackProps) => {
    const {
      hasCamera,
      permissionToUseCamera,
      flashIsAvailable,
      previewHeightPercent,
      previewWidthPercent,
    } = deviceDetails;
    setScannerState((prevState) => ({
      ...prevState,
      loadingCamera: false,
      device: {
        initialized: true,
        hasCamera,
        permissionToUseCamera,
        flashIsAvailable,
        previewHeightPercent: previewHeightPercent || 1,
        previewWidthPercent: previewWidthPercent || 1,
      },
    }));
  };

  // On some android devices, the aspect ratio of the preview is different than
  // the screen size. This leads to distorted camera previews. This allows for correcting that.
  const getPreviewSize = () => {
    const dimensions = Dimensions.get('window');
    // We use set margin amounts because for some reasons the percentage values don't align the camera preview in the center correctly.
    const heightMargin =
      ((1 - scannerState.device.previewHeightPercent) * dimensions.height) / 2;
    const widthMargin =
      ((1 - scannerState.device.previewWidthPercent) * dimensions.width) / 2;
    if (dimensions.height > dimensions.width) {
      // Portrait
      return {
        height: scannerState.device.previewHeightPercent,
        width: scannerState.device.previewWidthPercent,
        marginTop: heightMargin,
        marginLeft: widthMargin,
      };
    }

    // Landscape
    return {
      width: scannerState.device.previewHeightPercent,
      height: scannerState.device.previewWidthPercent,
      marginTop: widthMargin,
      marginLeft: heightMargin,
    };
  };

  // Renders the camera controls or a loading/processing state
  const renderCameraOverlay = () => {
    let loadingState = null;
    if (scannerState.loadingCamera) {
      loadingState = (
        <View style={styles.overlay}>
          <View style={styles.loadingContainer}>
            <ActivityIndicator color="white" />
            <Text style={styles.loadingCameraMessage}>Loading Camera</Text>
          </View>
        </View>
      );
    } else if (scannerState.processingImage) {
      loadingState = (
        <View style={styles.overlay}>
          <View style={styles.loadingContainer}>
            <View style={styles.processingContainer}>
              <ActivityIndicator color="#333333" size="large" />
              <Text style={{ color: '#333333', fontSize: 30, marginTop: 10 }}>
                Processing
              </Text>
            </View>
          </View>
        </View>
      );
    }

    return (
      <>
        {loadingState}
        <SafeAreaView style={[styles.overlay]}>
          {renderCameraControls()}
        </SafeAreaView>
      </>
    );
  };

  // Renders either the camera view, a loading state, or an error message
  // letting the user know why camera use is not allowed
  const renderCameraView = () => {
    if (scannerState.showScannerView) {
      const previewSize = getPreviewSize();
      let rectangleOverlay = null;
      if (!scannerState.loadingCamera && !scannerState.processingImage) {
        rectangleOverlay = (
          <RectangleOverlay
            detectedRectangle={scannerState.detectedRectangle}
            previewRatio={previewSize}
            backgroundColor="rgba(255,181,6, 0.2)"
            borderColor="rgb(255,181,6)"
            borderWidth={4}
            // == These let you auto capture and change the overlay style on detection ==
            // detectedBackgroundColor="rgba(255,181,6, 0.3)"
            // detectedBorderWidth={6}
            // detectedBorderColor="rgb(255,218,124)"
            // onDetectedCapture={this.capture}
            // allowDetection
          />
        );
      }

      // NOTE: I set the background color on here because for some reason the view doesn't line up correctly otherwise. It's a weird quirk I noticed.
      return (
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0)',
            position: 'relative',
            marginTop: previewSize.marginTop,
            marginLeft: previewSize.marginLeft,
            height: `${previewSize.height * 100}%`,
            width: `${previewSize.width * 100}%`,
          }}>
          <Scanner
            onPictureTaken={onScannerPictureTaken}
            onPictureProcessed={onScannerPictureProcessed}
            enableTorch={scannerState.flashEnabled}
            filterId={scannerState.filterId}
            ref={cameraRef}
            capturedQuality={0.6}
            onRectangleDetected={({ detectedRectangle }) =>
              setScannerState((prevState) => ({
                ...prevState,
                detectedRectangle,
              }))
            }
            onDeviceSetup={onDeviceSetup}
            onTorchChanged={({ enabled }) =>
              setScannerState((prevState) => ({
                ...prevState,
                flashEnabled: enabled,
              }))
            }
            style={styles.scanner}
          />
          {rectangleOverlay}
          <Animated.View
            style={{
              ...styles.overlay,
              backgroundColor: 'white',
              opacity: scannerState.overlayFlashOpacity,
            }}
          />
          {renderCameraOverlay()}
        </View>
      );
    }
  };

  return (
    <View
      style={styles.container}
      onLayout={(event) => {
        // This is used to detect multi tasking mode on iOS/iPad
        // Camera use is not allowed
        onLayout(event);
        if (scannerState.didLoadInitialLayout && Platform.OS === 'ios') {
          const screenWidth = Dimensions.get('screen').width;
          const isMultiTasking =
            Math.round(event.nativeEvent.layout.width) <
            Math.round(screenWidth);
          if (isMultiTasking) {
            setScannerState((prevState) => ({
              ...prevState,
              isMultiTasking: true,
              loadingCamera: false,
            }));
          } else {
            setScannerState((prevState) => ({
              ...prevState,
              isMultiTasking: false,
            }));
          }
        } else {
          setScannerState((prevState) => ({
            ...prevState,
            didLoadInitialLayout: true,
          }));
        }
      }}>
      <StatusBar
        backgroundColor="black"
        barStyle="light-content"
        hidden={Platform.OS !== 'android'}
      />
      {renderCameraView()}
    </View>
  );
};

export default DocumentScanner;
