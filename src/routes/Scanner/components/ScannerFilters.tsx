import * as React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Filter, Filters } from 'react-native-rectangle-scanner';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

interface Props {
  filterId: number;
  isMobile: boolean;
  onFilterIdChange: (id: number) => void;
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  group: {
    flex: 1,
    position: 'absolute',
    backgroundColor: '#00000080',
  },
  selector: {
    paddingHorizontal: 14,
    paddingVertical: 13,
    height: 50,
    width: 50,
  },
  listButton: {
    paddingHorizontal: 14,
    paddingVertical: 13,
    height: 50,
    width: 50,
  },
  touchable: {
    paddingHorizontal: 22,
    paddingVertical: 16,
  },
  touchableText: {
    flex: 1,
    color: 'white',
    fontSize: 13,
  },
  icon: {
    color: 'white',
    fontSize: 22,
    marginBottom: 3,
    textAlign: 'center',
  },
});

const ScannerFilters: React.FC<Props> = ({
  filterId,
  isMobile,
  onFilterIdChange,
}) => {
  const [filterMenuIsOpen, setFilterMenuIsOpen] = React.useState(false);

  const setFilter = (filter: Filter) => {
    onFilterIdChange(filter.id);
    setFilterMenuIsOpen(false);
  };

  const renderFilterOptions = () => {
    return Filters.RECOMMENDED_PLATFORM_FILTERS.map((f) => {
      return (
        <TouchableOpacity
          key={f.id}
          style={[
            styles.touchable,
            {
              width: f.name.length * 7 + 50,
            },
          ]}
          onPress={() => setFilter(f)}
          activeOpacity={0.8}>
          <Text
            numberOfLines={1}
            style={[
              styles.touchableText,
              f.id === (filterId || 1) ? { color: 'yellow' } : null,
            ]}>
            {f.name}
          </Text>
        </TouchableOpacity>
      );
    });
  };

  return (
    <View style={styles.container}>
      {filterMenuIsOpen ? (
        <View
          style={[
            styles.group,
            {
              borderRadius: isMobile ? 17 : 30,
              flexDirection: isMobile ? 'column' : 'row',
              right: isMobile ? 0 : 75,
              bottom: isMobile ? 75 : 8,
            },
          ]}>
          {renderFilterOptions()}
        </View>
      ) : null}
      <View style={styles.listButton}>
        <TouchableOpacity
          style={{}}
          onPress={() => setFilterMenuIsOpen(!filterMenuIsOpen)}
          activeOpacity={0.8}>
          <MaterialIcons name="gradient" style={styles.icon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ScannerFilters;
