import { StyledText } from 'components/StyledText';
import ScrollLayout from 'layouts/scroll-layout';
import * as React from 'react';
import { Button, StyleSheet, TextInput, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'stores/redux';
import { changeTheme } from 'stores/redux/modules/preferences';

interface Props {}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  header: {
    backgroundColor: 'white',
    height: 49,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  textInput: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
});

const Dashboard: React.FC<Props> = ({}) => {
  const dispatch = useDispatch();
  const { themeSelection, receiptCategory } = useSelector(
    (state: RootState) => state.preferences,
  );
  const [theme, setTheme] = React.useState<string>('');

  const handleThemeSubmit = () => {
    dispatch(changeTheme(theme));
    setTheme('');
  };

  const handleThemeInputChange = (text: string) => {
    setTheme(text);
  };

  return (
    <ScrollLayout>
      <View style={styles.container}>
        <View style={styles.header}>
          <StyledText>Dashboard</StyledText>
        </View>
        <StyledText>{`Theme - ${themeSelection}`}</StyledText>
        <TextInput
          style={styles.textInput}
          value={theme}
          placeholder="Theme"
          placeholderTextColor="#9a73ef"
          onChangeText={(text) => {
            handleThemeInputChange(text);
          }}
        />
        <Button title="submit" onPress={handleThemeSubmit} />
      </View>
    </ScrollLayout>
  );
};

export default Dashboard;
