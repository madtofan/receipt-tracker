import { DateRangeType, DateRangeValue } from 'constants/types';
import ScrollLayout from 'layouts/scroll-layout';
import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { realmStore } from 'stores/realm';
import { IReceiptModel } from 'stores/realm/models/receiptModel';
import { RootState } from 'stores/redux';
import { changeDateRange, changePage } from 'stores/redux/modules/receiptPages';
import { getWholeDurationFromTo } from 'utils/dates';

interface Props {}

interface PageDetail {
  dateStart: Date;
  dateEnd: Date;
  content: Realm.Results<IReceiptModel>;
}

const styles = StyleSheet.create({
  container: {},
});

const BrowseByDate: React.FC<Props> = ({}) => {
  const dispatch = useDispatch();
  const { currentDate, dateRange } = useSelector(
    (state: RootState) => state.receiptPages,
  );
  const [receiptPage, setReceiptPage] = React.useState<PageDetail>({
    dateStart: new Date(),
    dateEnd: new Date(),
    content: null,
  });

  React.useEffect(() => {
    handlePageLoad(currentDate, dateRange);
  }, []);

  const handlePageLoad = (date: Date, range: DateRangeType) => {
    const currentDateRange = DateRangeValue.get(range);
    const { from, to } = getWholeDurationFromTo(
      currentDate,
      currentDateRange.constructor,
    );
    setReceiptPage({
      dateStart: from,
      dateEnd: to,
      content: realmStore.receipt.retrieveReceiptByDuration(
        date,
        currentDateRange.constructor,
      ),
    });
  };

  const handleDateRangeUpdate = (newRange: DateRangeType) => {
    dispatch(changeDateRange(newRange));
    handlePageLoad(currentDate, newRange);
  };

  const handleDateChange = (newDate: Date) => {
    dispatch(changePage(newDate));
    handlePageLoad(newDate, dateRange);
  };

  return (
    <ScrollLayout scrollable={false}>
      <View></View>
    </ScrollLayout>
  );
};

export default BrowseByDate;
