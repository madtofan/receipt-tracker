import { StyledText } from 'components/StyledText';
import ScrollLayout from 'layouts/scroll-layout';
import * as React from 'react';
import { Button, StyleSheet, TextInput, View } from 'react-native';
import { realmStore } from 'stores/realm';
import { IReceiptModel } from 'stores/realm/models/receiptModel';

interface Props {}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  header: {
    backgroundColor: 'white',
    height: 49,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  content: {
    flexGrow: 1,
    backgroundColor: 'white',
  },
  textInput: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
});

const AddReceipt: React.FC<Props> = () => {
  const defaultReceipt: IReceiptModel = {
    datePurchased: new Date(),
    dateUploaded: new Date(),
    lastUpdated: new Date(),
    records: null,
    description: '',
    storeName: '',
  };
  const [receipt, setReceipt] = React.useState<IReceiptModel>(defaultReceipt);

  const handleInputChange = (text: string, parameter: string) => {
    setReceipt((oldState) => ({ ...oldState, [parameter]: text }));
  };

  const handleSubmitReceipt = () => {
    realmStore.receipt.saveReceipt(receipt);
    setReceipt(defaultReceipt);
  };

  return (
    <ScrollLayout>
      <View style={styles.container}>
        <View style={styles.header}>
          <StyledText>Add Receipt</StyledText>
        </View>
        <View style={styles.content}>
          <TextInput
            style={styles.textInput}
            value={receipt.description}
            placeholder="Description"
            placeholderTextColor="#9a73ef"
            onChangeText={(text) => {
              handleInputChange(text, 'description');
            }}
          />
          <TextInput
            style={styles.textInput}
            value={receipt.storeName}
            placeholder="Store name"
            placeholderTextColor="#9a73ef"
            onChangeText={(text) => {
              handleInputChange(text, 'storeName');
            }}
          />
          <Button title="submit" onPress={handleSubmitReceipt} />
        </View>
      </View>
    </ScrollLayout>
  );
};

export default AddReceipt;
