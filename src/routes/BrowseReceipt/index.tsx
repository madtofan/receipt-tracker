import ScrollLayout from 'layouts/scroll-layout';
import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface Props {}

const styles = StyleSheet.create({
  container: { width: '100%', height: '100%' },
});

const BrowseReceipt: React.FC<Props> = ({}) => {
  return (
    <View style={styles.container}>
      <ScrollLayout>
        <Text>This is a test browse page</Text>
      </ScrollLayout>
    </View>
  );
};

export default BrowseReceipt;
