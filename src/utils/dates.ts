import moment, { DurationInputArg1, unitOfTime } from 'moment';

interface DateFromTo {
  from: Date;
  to: Date;
}

export const getWholeDurationFromTo = (
  date: Date,
  constructor: unitOfTime.DurationConstructor,
): DateFromTo => {
  const momentDate = moment(date);
  return {
    from: momentDate.startOf(constructor).toDate(),
    to: momentDate.endOf(constructor).toDate(),
  };
};

export const getNextDate = (
  date: Date,
  value: DurationInputArg1,
  constructor: unitOfTime.DurationConstructor,
): Date => {
  return moment(date).add(value, constructor).toDate();
};

export const getPrevDate = (
  date: Date,
  value: DurationInputArg1,
  constructor: unitOfTime.DurationConstructor,
): Date => {
  return moment(date).subtract(value, constructor).toDate();
};
