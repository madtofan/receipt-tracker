import { DurationInputArg1, unitOfTime } from 'moment';

export type DateRangeType = 'Daily' | 'Weekly' | 'Monthly' | 'Yearly';

interface DurationType {
  constructor: unitOfTime.DurationConstructor;
  value: DurationInputArg1;
}

export const DateRangeValue: Map<DateRangeType, DurationType> = new Map<
  DateRangeType,
  DurationType
>([
  ['Daily', { constructor: 'days', value: 1 }],
  ['Weekly', { constructor: 'weeks', value: 1 }],
  ['Monthly', { constructor: 'months', value: 1 }],
  ['Yearly', { constructor: 'years', value: 1 }],
]);
