export const ROOT_PATH = '/';
export const BROWSE_PATH = '/browse';
export const DASHBOARD_PATH = '/dashboard';
export const SCANNER_PATH = '/scanner';
export const RECEIPTS_PATH = '/receipts';
