export type ColorType = 'Basic' | 'Grayscale';

interface ColorLevel {
  primary: string;
  secondary: string;
}

export interface ColorScheme {
  colorType: ColorType;
  background: ColorLevel;
  font: ColorLevel;
  icon: ColorLevel;
  button: string;
  buttonActive: ColorLevel;
  border: string;
}

const BASIC_MODE: ColorScheme = {
  colorType: 'Basic',
  background: { primary: '#E7DACD', secondary: '#CEC1B6' },
  font: { primary: '#2B1C14', secondary: '#60574E' },
  icon: { primary: '#4B4137', secondary: '#F3EFEA' },
  button: '#C08F5D',
  buttonActive: { primary: '#915909', secondary: '#6E3C07' },
  border: '#4B4137',
};

const GRAYSCALE_MODE: ColorScheme = {
  colorType: 'Grayscale',
  background: { primary: '#FFFFFF', secondary: '#F1F1F1' },
  font: { primary: '#000000', secondary: '#858585' },
  icon: { primary: '#000000', secondary: '#858585' },
  button: '#FFFFFF',
  buttonActive: { primary: '#FFFFFF', secondary: '#DFDFDF' },
  border: '#BEBEBE',
};

export const COLORS: ColorScheme[] = [BASIC_MODE, GRAYSCALE_MODE];
