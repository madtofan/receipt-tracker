import { IUserModel } from "stores/realm/models/userModel";

export interface UserState extends IUserModel {
    realmObject: Realm.Object;
}

enum USER_ACTION_TYPE {
    CHANGE_USER_CONFIG = 'USER/CHANGE_USER_CONFIG',
}

interface ChangeUserConfig {
    readonly type: USER_ACTION_TYPE.CHANGE_USER_CONFIG,
    payload: {
        realmObject: Realm.Object,
        user: IUserModel,
    }
};

type UserAction = ChangeUserConfig;


// ACTIONS
export const changeUserConfig = (
    realmObject: Realm.Object,
    user: IUserModel
): UserAction => {
    return {
        type: USER_ACTION_TYPE.CHANGE_USER_CONFIG,
        payload: {
            realmObject: realmObject,
            user: user
        }
    }
}


// INITIAL STATE
const currentDate = new Date();
const initialState: UserState = {
    realmObject: null,
    userId: null,
    username: '',
    password: '',
    createdDate: currentDate,
    enabledSync: false,
    lastLogin: currentDate,
    receipts: [],
    records: []
};

// REDUCERS
export const userReducer = (
    state: UserState = initialState,
    action: UserAction,
): UserState => {
    switch (action.type) {
        case USER_ACTION_TYPE.CHANGE_USER_CONFIG: {
            const { payload } = <ChangeUserConfig>action;
            const userValues = payload.user;
            return {
                ...state,
                realmObject: payload.realmObject,
                userId: userValues.userId,
                username: userValues.username,
                password: userValues.password,
                createdDate: userValues.createdDate,
                enabledSync: userValues.enabledSync,
                lastLogin: userValues.lastLogin,
                receipts: userValues.receipts,
                records: userValues.records
            }
        }
        default: {
            return state;
        }
    }
}
