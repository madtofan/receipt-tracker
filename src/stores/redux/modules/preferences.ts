interface PreferencesState {
    receiptCategory: string[];
    themeSelection: string;
}

enum PREFERENCES_ACTION_TYPE {
    CHANGE_THEME = 'PREFERENCES/CHANGE_THEME',
    ADD_CATEGORY = 'PREFERENCES/ADD_CATEGORY',
    REMOVE_CATEGORY = 'PREFERENCES/REMOVE_CATEGORY',
    UPDATE_CATEGORY = 'PREFERENCES/UPDATE_CATEGORY',
}

interface ChangeTheme {
    readonly type: PREFERENCES_ACTION_TYPE.CHANGE_THEME;
    payload: {
        themeSelection: string;
    };
}

interface AddCategory {
    readonly type: PREFERENCES_ACTION_TYPE.ADD_CATEGORY;
    payload: {
        category: string;
    };
}

interface RemoveCategory {
    readonly type: PREFERENCES_ACTION_TYPE.REMOVE_CATEGORY;
    payload: {
        category: string;
    };
}

interface UpdateCategory {
    readonly type: PREFERENCES_ACTION_TYPE.UPDATE_CATEGORY;
    payload: {
        categories: string[];
    };
}

type PreferencesAction =
    | ChangeTheme
    | AddCategory
    | RemoveCategory
    | UpdateCategory;

// ACTIONS
export const changeTheme = (themeSelection: string): PreferencesAction => {
    return {
        type: PREFERENCES_ACTION_TYPE.CHANGE_THEME,
        payload: {
            themeSelection: themeSelection,
        },
    };
};

export const addCategory = (category: string): PreferencesAction => {
    return {
        type: PREFERENCES_ACTION_TYPE.ADD_CATEGORY,
        payload: {
            category: category,
        },
    };
};

export const removeCategory = (category: string): PreferencesAction => {
    return {
        type: PREFERENCES_ACTION_TYPE.REMOVE_CATEGORY,
        payload: {
            category: category,
        },
    };
};

export const updateCategory = (categories: string[]): PreferencesAction => {
    return {
        type: PREFERENCES_ACTION_TYPE.UPDATE_CATEGORY,
        payload: {
            categories: categories,
        },
    };
};

// INITIAL STATE
const initialState: PreferencesState = {
    receiptCategory: ['Groceries', 'Utility'],
    themeSelection: 'Dark Mode',
};

// REDUCERS
export const preferencesReducer = (
    state: PreferencesState = initialState,
    action: PreferencesAction,
): PreferencesState => {
    switch (action.type) {
        case PREFERENCES_ACTION_TYPE.CHANGE_THEME: {
            const { payload } = <ChangeTheme>action;
            return {
                ...state,
                themeSelection: payload.themeSelection,
            };
        }
        case PREFERENCES_ACTION_TYPE.ADD_CATEGORY: {
            const { payload } = <AddCategory>action;
            return {
                ...state,
                receiptCategory: [...state.receiptCategory, payload.category],
            };
        }
        case PREFERENCES_ACTION_TYPE.REMOVE_CATEGORY: {
            const { payload } = <RemoveCategory>action;
            const tempCategories = [...state.receiptCategory];
            return {
                ...state,
                receiptCategory: tempCategories.splice(
                    tempCategories.findIndex((category) => category === payload.category),
                    1,
                ),
            };
        }
        case PREFERENCES_ACTION_TYPE.UPDATE_CATEGORY: {
            const { payload } = <UpdateCategory>action;
            return {
                ...state,
                receiptCategory: payload.categories,
            };
        }
        default: {
            return state;
        }
    }
};
