import { DateRangeType, DateRangeValue } from 'constants/types';

interface ReceiptPagesState {
  dateRange: DateRangeType;
  currentDate: Date;
}

enum RECEIPT_PAGE_ACTION_TYPE {
  CHANGE_DATE = 'RECEIPT_PAGE/CHANGE_DATE',
  CHANGE_DATE_RANGE = 'RECEIPT_PAGE/CHANGE_DATE_RANGE',
}

interface ChangeDate {
  readonly type: RECEIPT_PAGE_ACTION_TYPE.CHANGE_DATE;
  payload: {
    date: Date;
  };
}

interface ChangeDateRange {
  readonly type: RECEIPT_PAGE_ACTION_TYPE.CHANGE_DATE_RANGE;
  payload: {
    dateRange: DateRangeType;
  };
}

type ReceiptPagesAction = ChangeDate | ChangeDateRange;

// ACTIONS
export const changePage = (date: Date): ReceiptPagesAction => {
  return {
    type: RECEIPT_PAGE_ACTION_TYPE.CHANGE_DATE,
    payload: {
      date: date,
    },
  };
};

export const changeDateRange = (
  dateRange: DateRangeType,
): ReceiptPagesAction => {
  return {
    type: RECEIPT_PAGE_ACTION_TYPE.CHANGE_DATE_RANGE,
    payload: {
      dateRange: dateRange,
    },
  };
};

// INITIAL STATE
const initialState: ReceiptPagesState = {
  dateRange: 'Monthly',
  currentDate: new Date(),
};

// REDUCERS
export const receiptPagesReducer = (
  state: ReceiptPagesState = initialState,
  action: ReceiptPagesAction,
): ReceiptPagesState => {
  let currentDateRange = DateRangeValue.get(state.dateRange);

  switch (action.type) {
    case RECEIPT_PAGE_ACTION_TYPE.CHANGE_DATE: {
      const { payload } = <ChangeDate>action;
      return {
        ...state,
        currentDate: payload.date,
      };
    }
    case RECEIPT_PAGE_ACTION_TYPE.CHANGE_DATE_RANGE: {
      const { payload } = <ChangeDateRange>action;
      currentDateRange = DateRangeValue.get(payload.dateRange);
      return {
        ...state,
        dateRange: payload.dateRange,
      };
    }
    default: {
      return state;
    }
  }
};
