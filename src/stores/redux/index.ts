import AsyncStorage from '@react-native-community/async-storage';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import { preferencesReducer } from './modules/preferences';
import { receiptPagesReducer } from './modules/receiptPages';
import { userReducer } from './modules/user';

const rootReducer = combineReducers({
  user: userReducer,
  preferences: preferencesReducer,
  receiptPages: receiptPagesReducer,
});
const persistedReducer = persistReducer(
  {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['receiptPages', 'user'],
  },
  rootReducer,
);

export const reduxStore = createStore(persistedReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof rootReducer>;
