import { ObjectSchema } from "realm";

export const RECORD_SCHEMA_NAME = 'RecordSchemaName';

export default class RecordModel implements IRecordModel {
    static schema: ObjectSchema = {
        name: RECORD_SCHEMA_NAME,
        primaryKey: 'recordId',
        properties: {
            recordId: { type: 'string' },
            user: 'UserSchemaName',
            receipt: 'ReceiptSchemaName',
            warrantyUntil: 'date?',
            dateClaimed: 'date?',
            price: { type: 'float?', default: 0, optional: true },
            category: 'string',
            status: 'string',
            description: { type: 'string?', default: '', optional: true },
            lastUpdated: 'date'
        }
    }

    public recordId: string;
    public receipt: Realm.Object;
    public warrantyUntil: Date;
    public dateClaimed: Date;
    public price: number;
    public category: string;
    public status: string;
    public description: string;
    public lastUpdated: Date;
}

export interface IRecordModel {
    recordId?: string,
    user?: Realm.Object,
    receipt: Realm.Object,
    warrantyUntil: Date,
    dateClaimed: Date,
    price?: number,
    category: string,
    status: string,
    description?: string,
    lastUpdated: Date,
}