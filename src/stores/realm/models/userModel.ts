import { ObjectSchema } from "realm";

export const USER_SCHEMA_NAME = 'UserSchemaName';

export default class UserModel implements IUserModel {
    static schema: ObjectSchema = {
        name: 'UserSchemaName',
        primaryKey: 'userId',
        properties: {
            userId: { type: 'string' },
            username: 'string',
            password: 'string',
            lastLogin: 'date',
            createdDate: 'date',
            enabledSync: 'bool',
            receipts: { type: 'linkingObjects', objectType: 'ReceiptSchemaName', property: 'user' },
            records: { type: 'linkingObjects', objectType: 'RecordSchemaName', property: 'user' },
        }
    }

    public userId: string;
    public username: string;
    public password: string;
    public lastLogin: Date;
    public createdDate: Date;
    public enabledSync: boolean;
    public receipts: Realm.Object[];
    public records: Realm.Object[];
}

export interface IUserModel {
    userId?: string,
    username: string,
    password: string,
    lastLogin: Date,
    createdDate: Date,
    enabledSync: boolean,
    receipts?: Realm.Object[]
    records?: Realm.Object[],
}