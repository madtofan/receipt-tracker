import { ObjectSchema } from "realm";

export const RECEIPT_SCHEMA_NAME = 'ReceiptSchemaName';

export default class ReceiptModel implements IReceiptModel {
    static schema: ObjectSchema = {
        name: 'ReceiptSchemaName',
        primaryKey: 'receiptId',
        properties: {
            receiptId: { type: 'string' },
            user: 'UserSchemaName',
            totalPrice: { type: 'float?', default: 0, optional: true },
            records: { type: 'linkingObjects', objectType: 'RecordSchemaName', property: 'receipt' },
            datePurchased: 'date',
            dateUploaded: 'date',
            image: { type: 'string?', default: '', optional: true },
            storeName: { type: 'string?', default: '', optional: true },
            description: { type: 'string?', default: '', optional: true },
            lastUpdated: 'date'
        }
    }

    public receiptId: string;
    public totalPrice: number;
    public records: Realm.Object[];
    public datePurchased: Date;
    public dateUploaded: Date;
    public image: string;
    public storeName: string;
    public description: string;
    public lastUpdated: Date;
}

export interface IReceiptModel {
    receiptId?: string,
    user?: Realm.Object,
    totalPrice?: number,
    records?: Realm.Object[],
    datePurchased: Date,
    dateUploaded: Date,
    image?: string,
    storeName?: string,
    description?: string,
    lastUpdated: Date,
}