import Realm from 'realm';
import receiptAction from './actions/receiptAction';
import recordAction from './actions/recordAction';
import userAction from './actions/userAction';
import ReceiptModel from './models/receiptModel';
import RecordModel from './models/recordModel';
import UserModel from './models/userModel';

const realmInstance = new Realm({
    path: 'receiptdb.realm',
    schema: [ReceiptModel, RecordModel, UserModel],
    schemaVersion: 0,
    migration: (oldRealm, newRealm) => { }
});

export const realmStore = {
    receipt: receiptAction(realmInstance),
    record: recordAction(realmInstance),
    user: userAction(realmInstance)
}