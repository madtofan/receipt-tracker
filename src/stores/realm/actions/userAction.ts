import { nanoid } from "nanoid";
import UserModel, { IUserModel, USER_SCHEMA_NAME } from "../models/userModel";

export interface UserAction {
    saveUser(receiptResponseModel: IUserModel): Promise<UserModel>;
    retrieveAllUsers(): UserModel[];
    retrieveSingleUser(primaryKey: string): Realm.Object;
}

export default (realmInstance: Realm): UserAction => {
    return {
        saveUser: (userResponse: IUserModel) => {
            const { username, password, enabledSync } = userResponse;
            return new Promise((resolve, reject) => {
                try {
                    const id = nanoid();
                    const currentDate = new Date();
                    const user: IUserModel = {
                        userId: id,
                        createdDate: currentDate,
                        enabledSync: enabledSync,
                        lastLogin: currentDate,
                        password: username,
                        username: password,
                        receipts: [],
                        records: []
                    }
                    realmInstance.write(() => {
                        resolve(realmInstance.create(USER_SCHEMA_NAME, user))
                    });
                } catch (error) {
                    resolve(error);
                }
            })
        },
        retrieveAllUsers: () => {
            return Array.from(realmInstance.objects(USER_SCHEMA_NAME));
        },
        retrieveSingleUser: (primaryKey: string) => {
            return realmInstance.objectForPrimaryKey(USER_SCHEMA_NAME, primaryKey)
        }
    }
}