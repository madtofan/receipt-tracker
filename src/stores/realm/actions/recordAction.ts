import { nanoid } from "nanoid";
import RecordModel, { IRecordModel, RECORD_SCHEMA_NAME } from "../models/recordModel";

export interface RecordActions {
    saveRecord(recordResponseModel: RecordModel): Promise<RecordModel>;
    retrieveAllRecords(): RecordModel[];
    retrieveSingleRecord(primaryKey: string): Realm.Object;
}

export default (realmInstance: Realm): RecordActions => {
    return {
        saveRecord: (recordResponse: IRecordModel) => {
            const { user, receipt, warrantyUntil, dateClaimed, price, category, status, description } = recordResponse;
            return new Promise((resolve, reject) => {
                try {
                    const id = nanoid();
                    const currentDate = new Date();
                    const record: IRecordModel = {
                        recordId: id,
                        user: user,
                        receipt: receipt,
                        warrantyUntil: warrantyUntil,
                        dateClaimed: dateClaimed,
                        price: price,
                        category: category,
                        status: status,
                        description: description,
                        lastUpdated: currentDate
                    }
                    realmInstance.write(() => {
                        resolve(realmInstance.create(RECORD_SCHEMA_NAME, record))
                    });
                } catch (error) {
                    resolve(error);
                }
            })
        },
        retrieveAllRecords: () => {
            return Array.from(realmInstance.objects(RECORD_SCHEMA_NAME));
        },
        retrieveSingleRecord: (primaryKey: string) => {
            return realmInstance.objectForPrimaryKey(RECORD_SCHEMA_NAME, primaryKey)
        }
    }
}