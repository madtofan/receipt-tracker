import { unitOfTime } from 'moment';
import { nanoid } from 'nanoid';
import { getWholeDurationFromTo } from 'utils/dates';
import ReceiptModel, {
  IReceiptModel,
  RECEIPT_SCHEMA_NAME,
} from '../models/receiptModel';

export interface ReceiptActions {
  saveReceipt(receiptResponseModel: IReceiptModel): Promise<ReceiptModel>;
  retrieveAllReceipts(): Realm.Results<IReceiptModel>;
  retrieveSingleReceipt(primaryKey: string): IReceiptModel;
  retrieveReceiptByDuration(
    startDate: Date,
    constructor: unitOfTime.DurationConstructor,
  ): Realm.Results<IReceiptModel>;
}

export default (realmInstance: Realm): ReceiptActions => {
  return {
    saveReceipt: (receiptResponse: IReceiptModel) => {
      const {
        totalPrice,
        datePurchased,
        description,
        storeName,
        image,
        user,
      } = receiptResponse;
      return new Promise((resolve, reject) => {
        try {
          const id = nanoid();
          const currentDate = new Date();
          const receipt: IReceiptModel = {
            receiptId: id,
            user: user,
            totalPrice: totalPrice,
            datePurchased: datePurchased,
            dateUploaded: currentDate,
            image: image,
            storeName: storeName,
            description: description,
            lastUpdated: currentDate,
            records: [],
          };
          realmInstance.write(() => {
            resolve(realmInstance.create(RECEIPT_SCHEMA_NAME, receipt));
          });
        } catch (error) {
          resolve(error);
        }
      });
    },
    retrieveAllReceipts: () => {
      return realmInstance.objects<IReceiptModel>(RECEIPT_SCHEMA_NAME);
    },
    retrieveSingleReceipt: (primaryKey: string) => {
      return realmInstance.objectForPrimaryKey<IReceiptModel>(
        RECEIPT_SCHEMA_NAME,
        primaryKey,
      );
    },
    retrieveReceiptByDuration: (
      startDate: Date,
      constructor: unitOfTime.DurationConstructor,
    ) => {
      const { from, to } = getWholeDurationFromTo(startDate, constructor);
      return realmInstance
        .objects<IReceiptModel>(RECEIPT_SCHEMA_NAME)
        .filtered('datePurchased >= $0 && datePurchased < $1', from, to);
    },
  };
};
