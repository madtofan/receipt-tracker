import * as React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import Modal from "react-native-modal";

interface Props {
    isVisible: boolean;
    onBackdropPress?: () => void;
}

const styles = StyleSheet.create({
    content: {
        backgroundColor: "#3F2B80",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    modalBackground: {
        flex: 1,
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "space-around",
        backgroundColor: "#FFFFFF",
    },
    activityIndicatorWrapper: {
        backgroundColor: "#3F2B80",
        height: 100,
        width: 100,
        borderRadius: 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around",
        alignSelf: "center",
    },
});

export const Loading: React.FC<Props> = ({ onBackdropPress, isVisible }) => {
    return (<Modal
        onBackdropPress={onBackdropPress}
        isVisible={isVisible}
        animationIn={"fadeIn"}
        animationOut={"fadeOut"}
    >
        <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={isVisible} />
        </View></Modal>);
}
