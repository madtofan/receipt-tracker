import * as React from 'react';
import { StyleProp, StyleSheet, Text, TextStyle } from 'react-native';

interface Props {
  style?: StyleProp<TextStyle>;
}

export const StyledText: React.FC<Props> = ({ style, children }) => {
  const styles = StyleSheet.create({
    text: { color: 'black' },
  });

  return <Text style={[styles.text, style]}>{children}</Text>;
};
