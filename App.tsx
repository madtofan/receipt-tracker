import { Loading } from 'components/Loading';
import { nanoid } from 'nanoid';
import * as React from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import Routes from 'routes';
import { realmStore } from 'stores/realm';
import { changeUserConfig } from 'stores/redux/modules/user';

interface Props {}

const App: React.FC<Props> = () => {
  const [loading, setLoading] = React.useState(true);
  const dispatch = useDispatch();

  React.useEffect(() => {
    const users = realmStore.user.retrieveAllUsers();
    if (users.length > 0) {
      const user = users[0];
      const userRealmObject = realmStore.user.retrieveSingleUser(user.userId);
      dispatch(changeUserConfig(userRealmObject, user));
    } else {
      realmStore.user
        .saveUser({
          username: nanoid(),
          password: nanoid(),
          lastLogin: new Date(),
          enabledSync: false,
          createdDate: new Date(),
        })
        .then((userModel) => {
          const user = userModel;
          const userRealmObject = realmStore.user.retrieveSingleUser(
            user.userId,
          );
          dispatch(changeUserConfig(userRealmObject, user));
        });
    }
    setLoading(false);
  }, []);

  return (
    <View>
      <Loading isVisible={loading} />
      <Routes />
    </View>
  );
};

export default App;
